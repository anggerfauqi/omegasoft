package com.example.omega;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    EditText username, password;
    Button b_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.et_username);
        password = findViewById(R.id.et_password);
        b_login = findViewById(R.id.btn_login);

        b_login.setOnClickListener(view -> auth());


    }

    public void auth() {
        if (username.getText().toString().equals("") || password.getText().toString().equals("")) {
            Toast.makeText(this, "Username and password cannot be empty!", Toast.LENGTH_LONG).show();
        } else {
            if (username.getText().toString().equals("user") && password.getText().toString().equals("123456")) {
                MainActivity.islogin = true;
                MainActivity.username = username.getText().toString();
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_LONG).show();
            }
        }
    }
}